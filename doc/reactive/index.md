# Reactive operations

This project runs a Ruby web service (a Rack application) which receives and react to webhooks from [the `gitlab-org` group](https://gitlab.com/gitlab-org), [the `gitlab-com/www-gitlab-com` project](https://gitlab.com/gitlab-com/www-gitlab-com/),
and [the `gitlab-com/runbooks` project](https://gitlab.com/gitlab-com/runbooks/).

For every webhook request, the service passes the event payload to each processors sequentially.

The list of processors can be found under [`triage/processor/`](triage/processor/).

## Why don't we just use quick actions?

Quick actions are processed by GitLab itself, and permissions are enforced. For instance, someone that's not a member of a project can post `/approve`, but the quick action won't be interpreted, as the user doesn't have sufficient permissions.

*Reactive operations* are a way to circumvent some of those permissions for specific commands. For instance, we want to allow community contributors to set a `group::*` label ([ReactiveLabeler](/triage/processor/reactive_labeler.rb)), request a review ([CommandRequestReview](/triage/processor/command_request_review.rb)), or directly assign a reviewer ([ReactiveReviewer](/triage/processor/reactive_reviewer.rb), only for JiHu contributors).

We also have some reactive commands that are *allowing actions that you cannot do with GitLab quick actions*, such as asking for help ([MergeRequestHelp](/triage/processor/merge_request_help.rb)), or giving feedback ([CodeReviewExperienceSlack](/triage/processor/code_review_experience_slack.rb)).

## Specific topics

- [Architecture](./architecture.md)
- [Run locally](./run_locally.md)
- [Implementing a new processor](./processor.md)
- [Production](./production.md)
- [Dashboard](./dashboard.md)
- [AppSec approval process](./appsec_approval_process.md)
