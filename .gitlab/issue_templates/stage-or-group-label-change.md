## Summary

This template is for the following types of label changes

* Adding a new Stage or Group
* Renaming an existing Stage or Group

For every case above, the Engineering Productivity team needs to ensure that:

* The label change is factored into the triage mechanism.
* Engineering Dashboards at <https://quality-dashboard.gitlap.com/groups/gitlab-org> are updated.
* Old labels are migrated correctly on affected issues and merge requests.

### Action items

* [ ] (If applicable) Dashboard creation: create or update the stage/group in <https://gitlab.com/gitlab-org/gitlab-insights/blob/master/lib/gitlab_insights.rb>.
* [ ] (If applicable) Label migration on existing issues and merge requests: apply the new label to opened & closed issues, and open & merged merge requests.
  * [ ] Check if label migration will apply labels which have subscribers.
    [Communicate with the subscribers](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#communicate-early-and-broadly-about-expected-automation-impact)
    before applying the label migration.
* [ ] (If applicable) Archive the old label with renaming and adding "DEPRECATED" at the end of the label name.
* [ ] (If applicable) Delete the old dashboard views using the deprecated labels.
* [ ] (If applicable) Update the group triage report definition to use the new label.
* [ ] (If applicable) Update the group label's description to refer to the new Stage.

/cc @gitlab-org/quality/engineering-productivity
/label ~Quality ~"label change"
