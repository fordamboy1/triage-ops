# frozen_string_literal: true

require_relative '../lib/threat_insights_helper'

Gitlab::Triage::Resource::Context.include ThreatInsightsHelper
