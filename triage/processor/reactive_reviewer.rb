# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/rate_limit'

require 'digest'

module Triage
  # Assigns the given people (they need to be members of the gitlab-org group)
  # as reviewers to an MR, if the MR author is from JiHu.
  class ReactiveReviewer < Processor
    include RateLimit

    REVIEWERS_REGEX = /@([^@ ]+)/.freeze

    react_to 'merge_request.note'
    define_command name: 'assign_reviewer', args_regex: REVIEWERS_REGEX

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        event.jihu_contributor? &&
        command.valid?(event) &&
        valid_reviewers?
    end

    def process
      post_assign_reviewer_command
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("reactive-reviewer-commands-sent-#{event.event_actor_id}")
    end

    private

    def valid_reviewers?
      valid_reviewers.any?
    end

    def valid_reviewers
      @valid_reviewers ||= command.args(event) & Triage.gitlab_org_group_member_usernames
    end

    def post_assign_reviewer_command
      add_comment <<~MARKDOWN.chomp
        /assign_reviewer #{reviewer_mentions}
      MARKDOWN
    end

    def reviewer_mentions
      valid_reviewers.map { |reviewer| "@#{reviewer}"}.join(' ')
    end
  end
end
