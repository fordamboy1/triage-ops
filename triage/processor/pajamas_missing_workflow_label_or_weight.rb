# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'

module Triage
  class PajamasMissingWorkflowLabelOrWeight < Processor
    PAJAMAS_PROJECT_ID = 4456656

    react_to 'issue.open', 'issue.update'

    def applicable?
      is_pajamas_project? &&
        (workflow_label_not_added? || weight_not_added?) &&
        unique_comment.no_previous_comment?
    end

    def process
      post_missing_label_weight_comment
    end

    private

    def is_pajamas_project?
      event.with_project_id?(PAJAMAS_PROJECT_ID)
    end

    def workflow_label_not_added?
      event.label_names.none? { |label| label.start_with?('workflow::') }
    end

    def weight_not_added?
      event.weight.nil?
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def post_missing_label_weight_comment
      add_comment(message.strip)
    end

    def message
      comment =  <<~MARKDOWN.chomp
        Hi @#{event.event_actor_username},
          
        Please make sure that both a [workflow label](https://about.gitlab.com/handbook/product-development-flow/) and [weight](https://about.gitlab.com/handbook/engineering/ux/product-designer/#ux-issue-weights) are added to your issue. This helps to categorize and locate issues in the future.

        Thanks for your help! :heart:
      MARKDOWN

      unique_comment.wrap(comment)
    end
  end
end
