# frozen_string_literal: true

require 'time'
require_relative '../triage/processor'

module Triage
  class HackathonLabel < Processor
    HACKATHON_START_DATE = Time.parse('2022-05-09T12:00:00Z')
    HACKATHON_END_DATE = Time.parse('2022-05-13T12:00:00Z')
    HACKATHON_TRACKING_ISSUE = 'https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/-/issues/65'
    HACKATHON_WEBPAGE = 'https://about.gitlab.com/community/hackathon/'

    react_to 'merge_request.*'

    def applicable?
      gitlab_org_or_gitlab_com? &&
        created_during_hackathon? &&
        community_contribution_label_added?
    end

    def process
      label_hackathon
    end

    private

    def label_hackathon
      add_comment(<<~MARKDOWN.chomp)
        This merge request will be considered [part of](#{HACKATHON_TRACKING_ISSUE}) the quarterly [GitLab Hackathon](#{HACKATHON_WEBPAGE}) for a chance to win a [prize](#{HACKATHON_WEBPAGE}#prize).

        Can you make sure this merge request mentions or links to the relevant issue that it's attempting to close?

        Thank you for your contribution!

        /label ~Hackathon
      MARKDOWN
    end

    def gitlab_org_or_gitlab_com?
      event.from_gitlab_org? || event.from_gitlab_com?
    end

    def community_contribution_label_added?
      event.added_label_names.include?('Community contribution')
    end

    def created_during_hackathon?
      event.created_at >= HACKATHON_START_DATE &&
        event.created_at < HACKATHON_END_DATE
    end
  end
end
