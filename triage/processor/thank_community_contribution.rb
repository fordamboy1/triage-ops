# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../strings/thanks'

module Triage
  class ThankCommunityContribution < Processor
    GITLAB_RUNNER_PROJECT_ID = 250_833
    WWW_GITLAB_COM_PROJECT_ID = 7764
    GITLAB_PROJECT_ID = 278_964

    PROJECT_THANKS = {
      GITLAB_RUNNER_PROJECT_ID => {
        message: Strings::Thanks::RUNNER_THANKS
      },
      WWW_GITLAB_COM_PROJECT_ID => {
        extra_conditions: ->(event) {
          event.from_gitlab_com? &&
          event.with_project_id?(WWW_GITLAB_COM_PROJECT_ID) &&
          event.wider_gitlab_com_community_author?
        },
        message: Strings::Thanks::WWW_GITLAB_COM_THANKS
      },
      GITLAB_PROJECT_ID => {
        message: Strings::Thanks::GITLAB_THANKS
      }
    }

    DEFAULT_EXTRA_CONDITIONS = ->(event) {
      event.from_gitlab_org? &&
      event.wider_community_author?
    }

    react_to 'merge_request.open'

    def applicable?
      extra_conditions
    end

    def process
      post_thank_you_message
    end

    private

    def post_thank_you_message
      thank_you_message = PROJECT_THANKS.dig(event.project_id, :message) || Strings::Thanks::DEFAULT_THANKS
      add_comment format(thank_you_message, author_username: event.event_actor_username)
    end

    def extra_conditions
      extra_conditions_lambda = PROJECT_THANKS.dig(event.project_id, :extra_conditions)
      extra_conditions_lambda ? extra_conditions_lambda.call(event) : DEFAULT_EXTRA_CONDITIONS.call(event)
    end
  end
end
