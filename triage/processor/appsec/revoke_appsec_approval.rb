# frozen_string_literal: true

require_relative '../appsec_processor'

module Triage
  class RevokeAppSecApproval < AppSecProcessor
    react_to 'merge_request.update' # We care about push which is update

    def applicable?
      super && event.revision_update? && has_appsec_approval_label?
    end

    def process
      if appsec_discussion_thread
        discussion_id = appsec_discussion_thread['id']

        # Make sure we unresolve as soon as possible
        unresolve_discussion(discussion_id)
        append_discussion(message, discussion_id)
      else
        add_discussion(message)
      end
    end

    private

    def message
      @message ||= unique_comment.wrap(<<~MARKDOWN.strip)
        :x: Some new changes have been pushed since AppSec last approved.

        Can you please review again #{appsec_approver_username_formatted}?

        To approve this again, please un-approve and approve again.
        ~"#{APPSEC_APPROVAL_LABEL}" will be added automatically after
        being re-approved. Here's the quick actions to do this:

        ```
        /unapprove
        /approve
        ```

        #{quick_actions}
      MARKDOWN
    end

    def appsec_approver_username_formatted
      "@#{appsec_approver['username']}"
    end

    def quick_actions
      # We still unlabel APPSEC_APPROVAL_LABEL in case scopes are changed
      # If they share the same scope it should be a no-op
      <<~MARKDOWN.strip
        /label ~"#{APPSEC_PENDING_APPROVAL_LABEL}"
        /unlabel ~"#{APPSEC_APPROVAL_LABEL}"
      MARKDOWN
    end

    def appsec_approver
      @appsec_approver ||= approvers.find do |approver|
        approved_by_appsec?(approver['id'])
      end || appsec_group
    end

    def appsec_group
      { 'username' => Triage::GITLAB_COM_APPSEC_GROUP }
    end

    def approvers
      approvals =
        Triage.api_client.merge_request_approvals(event.project_id, event.iid)

      approvals.to_h['approved_by'].filter_map do |approval|
        approval['user']&.slice('id', 'username')
      end
    end
  end
end
