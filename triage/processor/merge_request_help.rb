# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/rate_limit'

require_relative '../../lib/www_gitlab_com'
require_relative '../../lib/merge_request_coach_helper'

require 'digest'

module Triage
  class MergeRequestHelp < Processor
    include RateLimit
    include MergeRequestCoachHelper

    react_to 'merge_request.note'
    define_command name: 'help'

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        command.valid?(event)
    end

    def process
      add_comment <<~MARKDOWN.chomp
        Hey there #{select_random_merge_request_coach}, could you please take a look at this merge request and help @#{event.event_actor_username} out?

        :wave: @#{event.event_actor_username},

        Thanks for reaching out for help. I've notified [the merge request coaches](https://about.gitlab.com/company/team/?department=merge-request-coach).

        They will get back to you as soon as they can.

        If you have not received any response, you may ask for help again after 1 day.
      MARKDOWN
    end

    private

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("help-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end
  end
end
