# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class RemovePingpongLabelOnAuthorActivity < Processor
    PINGPONG_EMOJI = "🏓".freeze

    react_to 'merge_request.note'

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        event.label_names.include?(PINGPONG_EMOJI)
    end

    def process
      add_comment(%Q{/unlabel ~"#{PINGPONG_EMOJI}"})
    end
  end
end
