# frozen_string_literal: true

require 'rack'

require_relative '../triage/event'

module Triage
  module Rack
    class WebhookEvent < Struct.new(:app)
      def call(env)
        payload = JSON.parse("[#{::Rack::Request.new(env).body.read}]").first || {}

        app.call(env.merge(payload: payload))
      rescue JSON::ParserError => error
        ::Rack::Response.new([JSON.dump(status: :error, error: error.class, message: error.message)], 400).finish
      end
    end
  end
end
