# frozen_string_literal: true

require_relative '../triage/sucker_punch'
require_relative '../triage'

module Triage
  class Job
    include SuckerPunch::Job

    attr_reader :event

    def self.drain!
      pool = SuckerPunch::Queue::QUEUES.delete(to_s)

      return true unless pool

      # See https://github.com/brandonhilkert/sucker_punch/blob/v3.0.1/lib/sucker_punch/queue.rb#L74
      deadline = Time.now + SuckerPunch.shutdown_timeout

      pool.shutdown

      remaining = deadline - Time.now

      while remaining > SuckerPunch::Queue::PAUSE_TIME
        break if pool.shutdown?
        sleep SuckerPunch::Queue::PAUSE_TIME
        remaining = deadline - Time.now
      end

      remaining > 0
    end

    def perform(...)
      start_time = Triage.current_monotonic_time

      execute(...)
    ensure
      duration = (Triage.current_monotonic_time - start_time).round(5)

      end_executing(duration)
    end

    private

    def prepare_executing_with(event)
      @event = event
      logger.info('Executing job', job: self.class, resource_url: event.url)
    end

    def end_executing(duration)
      logger.info(
        'Executing job end',
        dry_run: Triage.dry_run?,
        job: self.class,
        duration: duration,
        event_class: event&.class,
        event_key: event&.key,
        event_payload: event&.payload
      )
    end
  end
end
