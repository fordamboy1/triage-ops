RSpec.shared_examples 'command processor' do |command, args_regex = nil|
  it 'registers the expected command' do
    expect(subject.command.name).to eq(command)
  end

  it 'registers the expected args_regex' do
    expect(subject.command.args_regex).to eq(args_regex)
  end
end
