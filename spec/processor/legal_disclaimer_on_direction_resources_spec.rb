# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/legal_disclaimer_on_direction_resources'
require_relative '../../triage/triage/event'

RSpec.describe Triage::LegalDisclaimerOnDirectionResources do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'open',
        from_gitlab_org?: true,
        project_id: project_id,
        iid: issue_iid,
        label_names: labels,
        description: description
      }
    end
    let(:project_id) { 123 }
    let(:issue_iid) { 300 }
    let(:labels) { ['direction'] }
    let(:description_without_legal_disclaimer) { "This is an issue description." }
    let(:description_with_legal_disclaimer) { "#{description_without_legal_disclaimer}\n#{described_class::LEGAL_DISCLAIMER}" }
    let(:description) { description_without_legal_disclaimer }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.update", "issue.open"]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when resource does not have direction label' do
      let(:labels) { [] }

      include_examples 'event is not applicable'
    end

    context 'when resource has the current legal disclaimer' do
      let(:description) { "#{described_class::LEGAL_DISCLAIMER}\n#{description_without_legal_disclaimer}" }

      include_examples 'event is not applicable'
    end

    context 'when resource has an old legal disclaimer' do
      let(:description) { "#{described_class::LEGAL_DISCLAIMER.gsub(/ v\d+ /, ' ')}\n#{description_without_legal_disclaimer}" }

      include_examples 'event is applicable'
    end
  end

  describe '#process' do
    it 'appends the legal disclaimer to the issue decription' do
      expect_api_request(verb: :put, path: "/projects/#{project_id}/issues/#{issue_iid}", request_body: { description: description_with_legal_disclaimer }) do
        subject.process
      end
    end

    context 'when resource has an old non-versioned disclaimer' do
      let(:description) { description_with_legal_disclaimer.gsub(/ v\d+ /, ' ') }

      it 'replaces the old disclaimer with the new one in the issue decription' do
        expect_api_request(verb: :put, path: "/projects/#{project_id}/issues/#{issue_iid}", request_body: { description: description_with_legal_disclaimer }) do
          subject.process
        end
      end
    end

    context 'when resource has an old versioned disclaimer' do
      let(:description) { description_with_legal_disclaimer.gsub(/ v\d+ /, ' v1 ') }

      it 'replaces the old disclaimer with the new one in the issue decription' do
        expect_api_request(verb: :put, path: "/projects/#{project_id}/issues/#{issue_iid}", request_body: { description: description_with_legal_disclaimer }) do
          subject.process
        end
      end
    end
  end
end
