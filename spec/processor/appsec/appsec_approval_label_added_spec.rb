# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/appsec/appsec_approval_label_added'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::AppSecApprovalLabelAdded, :clean_cache do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:actor_username) { 'label_adder' }
    let(:actor_user_id) { 4242 }
    let(:appsec_username) { actor_username }
    let(:appsec_user_id) { actor_user_id }
    let(:added_label_names) { [Triage::AppSecProcessor::APPSEC_APPROVAL_LABEL] }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org?: true,
        jihu_contributor?: true,
        gitlab_bot_event_actor?: false,
        event_actor_username: actor_username,
        event_actor_id: actor_user_id,
        last_commit_sha: 'deadbeaf',
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/groups/#{CGI.escape(Triage::GITLAB_COM_APPSEC_GROUP)}/members",
      query: { per_page: 100 },
      response_body: [username: appsec_username, id: appsec_user_id])
  end

  include_examples 'registers listeners',
    ['merge_request.update', 'merge_request.open']

  describe '#applicable' do
    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request is not a JiHu contribution' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request does not have the approval label added' do
      let(:added_label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when the label is added by @gitlab-bot' do
      before do
        allow(event).to receive(:gitlab_bot_event_actor?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request have the approval label added' do
      include_examples 'event is applicable'
    end
  end

  describe '#process' do
    let(:discussion_id) { 'approval_discussion_id' }
    let(:discussions_path) { "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions" }
    let(:approval_discussion_path) { "#{discussions_path}/#{discussion_id}" }

    let(:merge_request_notes) do
      [
        { body: 'review comment 1' },
        { body: comment_mark }
      ]
    end

    let(:merge_request_discussions) do
      [ id: discussion_id, notes: merge_request_notes ]
    end

    context 'when there is already a AppSec thread' do
      before do
        stub_api_request(
          path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions",
          query: { per_page: 100 },
          response_body: merge_request_discussions)
      end

      it 'appends a new note to the discussion and resolves it' do
        expect_api_requests do |requests|
          # Appends the new note to the approval discussion
          requests << stub_api_request(
            verb: :post,
            path: "#{approval_discussion_path}/notes",
            request_body: { body: <<~MARKDOWN.strip })
            #{comment_mark}
            :white_check_mark: AppSec added ~"sec-planning::complete" for
            deadbeaf by `@#{event.event_actor_username}`
          MARKDOWN

          # Resolve the approval discussion
          requests << stub_api_request(verb: :put, path: approval_discussion_path, request_body: { 'resolved' => true })

          subject.process
        end
      end

      context 'when the label is not added by AppSec' do
        let(:appsec_username) { actor_username.swapcase }
        let(:appsec_user_id) { actor_user_id.succ }

        it 'appends a new note to explain why it should not happen and removes the label' do
          expect_api_requests do |requests|
            # Un-resolve the approval discussion
            requests << stub_api_request(verb: :put, path: approval_discussion_path, request_body: { 'resolved' => false })

            # Appends the new note explain why and remove the label
            requests << stub_api_request(
              verb: :post,
              path: "#{approval_discussion_path}/notes",
              request_body: { body: <<~MARKDOWN.strip })
              #{comment_mark}
              @label_adder Please don't add ~"sec-planning::complete"
              because it's supposed to be added by
              `@gitlab-com/gl-security/appsec` or automation only.

              /unlabel ~"sec-planning::complete"
            MARKDOWN

            subject.process
          end
        end
      end
    end

    context 'when there is no AppSec thread' do
      before do
        stub_api_request(
          path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions",
          query: { per_page: 100 },
          response_body: [])
          .times(1).then
          .to_return(body: JSON.dump(merge_request_discussions))
      end

      it 'creates a new discussion and resolves it' do
        expect_api_requests do |requests|
          # Creates a new discussion
          requests << stub_api_request(
            verb: :post,
            path: discussions_path,
            request_body: { body: <<~MARKDOWN.strip })
            #{comment_mark}
            :white_check_mark: AppSec added ~"sec-planning::complete" for
            deadbeaf by `@#{event.event_actor_username}`
          MARKDOWN

          # Resolve the approval discussion
          requests << stub_api_request(verb: :put, path: approval_discussion_path, request_body: { 'resolved' => true })

          subject.process
        end
      end
    end
  end
end
