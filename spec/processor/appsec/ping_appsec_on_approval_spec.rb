# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/appsec/ping_appsec_on_approval'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::PingAppSecOnApproval do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:approver_username) { 'approver' }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org?: true,
        jihu_contributor?: true,
        gitlab_bot_event_actor?: false,
        event_actor_username: approver_username,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners',
    ['merge_request.approval', 'merge_request.approved']

  describe '#applicable?' do
    context 'when there is no previous comment to ping AppSec' do
      include_examples 'event is applicable'
    end

    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request is not a JiHu contribution' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { "body": "review comment 1" },
          { "body": comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    context 'when merge request author is a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(true)
      end

      it 'posts a discussion to request AppSec review' do
        body = <<~MARKDOWN.chomp
          #{comment_mark}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request is approved. Please wait for AppSec approval.

          cc @gitlab-com/gl-security/appsec this is a ~"JiHu contribution", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-security-review-process.html#security-review-workflow-for-jihu-contributions).
        MARKDOWN

        expect_discussion_request(event: event, body: body) do
          subject.process
        end
      end

      context 'when the merge request is already approved by AppSec' do
        let(:label_names) { [described_class::APPSEC_APPROVAL_LABEL] }

        include_examples 'event is not applicable'
      end
    end
  end
end
