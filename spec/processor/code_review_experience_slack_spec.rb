# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/code_review_experience_slack'

RSpec.describe Triage::CodeReviewExperienceSlack do
  include_context 'slack posting context'
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: true,
        wider_community_author?: true,
        by_noteable_author?: true,
        new_comment: %(@gitlab-bot feedback),
        url: 'https://comment.url/note#123'
      }
    end
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    allow(messenger_stub).to receive(:ping)
  end

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', 'feedback'
  it_behaves_like 'processor slack options', '#mr-feedback'

  describe '#applicable?' do
    context 'when event is from gitlab org, for a note on a new merge request, by the mr author' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event author is not the noteable author' do
      before do
        allow(event).to receive(:by_noteable_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it_behaves_like 'slack message posting' do
      let(:message_body) do
        <<~MARKDOWN
          Hola MR coaches, a contributor has left feedback about their experience in #{event.url}.
        MARKDOWN
      end
    end
  end
end
