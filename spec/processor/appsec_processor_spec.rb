# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/appsec_processor'

RSpec.describe Triage::AppSecProcessor do
  let(:from_gitlab_org) { false }
  let(:jihu_contributor) { false }
  let(:gitlab_bot_event_actor) { false }

  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org?: from_gitlab_org,
        jihu_contributor?: jihu_contributor,
        gitlab_bot_event_actor?: gitlab_bot_event_actor
      }
    end
  end

  subject { described_class.new(event) }

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event project is under gitlab-org' do
      let(:from_gitlab_org) { true }

      context 'but is not a JiHu contribution' do
        let(:jihu_contributor) { false }

        include_examples 'event is not applicable'
      end

      context 'and it has ~"JiHu contribution" label' do
        let(:label_names) { 'JiHu contribution' }

        include_examples 'event is applicable'

        context 'but authored by @gitlab-bot' do
          let(:gitlab_bot_event_actor) { true }

          include_examples 'event is not applicable'
        end
      end

      context 'and is from a JiHu contributor' do
        let(:jihu_contributor) { true }

        include_examples 'event is applicable'
      end
    end
  end
end
