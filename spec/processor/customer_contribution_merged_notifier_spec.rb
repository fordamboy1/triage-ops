# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/customer_contribution_merged_notifier'
require_relative '../../triage/triage/event'

RSpec.describe Triage::CustomerContributionMergedNotifier do
  include_context 'slack posting context'
  include_context 'with event' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'merge',
        from_gitlab_org?: true,
        wider_community_author?: true,
        type_label: contribution_type,
        url: url
      }
    end
    let(:contribution_type) { 'type::bug' }
    let(:url) { 'http://gitlab.com/mr_url' }
  end

  let(:org_name) { 'org' }

  subject { described_class.new(event, messenger: messenger_stub) }

  include_examples 'registers listeners', ['merge_request.merge']

  let(:message_body) do
    <<~MARKDOWN
      > Customer MR Merged -
      > Organization: #{described_class::CUSTOMER_PORTAL_URL}#{org_name}
      > Contribution Type: #{contribution_type}
      > MR Link: #{url}
    MARKDOWN
  end

  before do
    allow(Triage::OrgByUsernameLocator).to receive(:locate_org).and_return(org_name)
    allow(messenger_stub).to receive(:ping)
  end

  it_behaves_like 'processor slack options', '#mrarr-wins'
  it_behaves_like 'customer contribution processor #applicable?'

  describe '#process' do
    it_behaves_like 'customer contribution processor #process'
  end
end
