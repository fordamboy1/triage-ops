# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/apply_type_label_from_related_issue'
require_relative '../../triage/triage/event'

RSpec.describe Triage::ApplyTypeLabelFromRelatedIssue do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: true,
        type_label_set?: false,
        description: description,
        project_id: 12
      }
    end
    let(:description) do
      <<~TEXT
      Lorem ipsum

      Related to #42.
      TEXT
    end
    let(:type_label_from_related_issue) { "type::feature" }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when description does not include a related issue' do
      let(:description) { 'Hello world' }

      include_examples 'event is not applicable'
    end

    context 'when description includes a related issue' do
      include_examples 'event is applicable'
    end

    context 'when MR already has a type label' do
      before do
        allow(event).to receive(:type_label_set?).and_return(true)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    context 'when related issue has a type label' do
      before do
        allow(subject).to receive(:related_issue).and_return(double(labels: ['foo', type_label_from_related_issue]))
      end

      it 'posts a comment to add the detected type label' do
        body = <<~MARKDOWN.chomp
          /label ~"#{type_label_from_related_issue}"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when related issue has no type label' do
      before do
        allow(subject).to receive(:related_issue).and_return(double(labels: ['foo']))
      end

      it 'posts a comment to add the detected type label' do
        expect_no_request { subject.process }
      end
    end
  end
end
