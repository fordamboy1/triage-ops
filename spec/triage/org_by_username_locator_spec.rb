# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/org_by_username_locator'

RSpec.describe Triage::OrgByUsernameLocator, :clean_cache do
  let(:user_id) { 1 }
  let(:account_id) { '123abc' }
  let(:user_id_no_account) { 3 }
  let(:csv_body) do 
    <<~CSV
      USER_ID,CRM_ACCOUNT_ID
      1,123abc
      2,345def
      3,
      4,890xyz
    CSV
  end
  let(:csv_response) {
    double(parsed_response: csv_body)
  }

  before do
    allow(HTTParty).to receive(:get).and_return(csv_response)
  end

  describe '.locate_org' do
    subject { described_class.locate_org(user_id) }

    context 'invalid ENV var' do
      it 'raises an ArgumentError for attention if CSV_URL missing' do
        allow(ENV).to receive(:fetch).with(described_class::CSV_URL_VAR).and_return(nil)

        expect do
          described_class.locate_org(user_id)
        end.to raise_error(described_class::CsvUrlMissingError)
      end

      it 'raises an ArgumentError for attention if CSV_URL looks wrong' do
        allow(ENV).to receive(:fetch).with(described_class::CSV_URL_VAR).and_return("https://example.com/invalid.csv")

        expect do
          described_class.locate_org(user_id)
        end.to raise_error(described_class::CsvUrlInvalidError)
      end
    end

    context 'valid ENV var' do
      before do
        allow(ENV).to receive(:fetch).with(described_class::CSV_URL_VAR).and_return("https://app.periscopedata.com/api/gitlab/example.csv")
      end

      it 'caches the CSV URL response', :clean_cache do
        described_class.locate_org(user_id)

        expect(HTTParty).not_to receive(:get)

        described_class.locate_org(user_id)
      end

      it 'expires the cache and retrieves', :clean_cache do
        described_class.locate_org(user_id)

        # expire cache
        Timecop.travel(Time.now + described_class::USER_ORG_CSV_CACHE_EXPIRY + 1) do
          expect(HTTParty).to receive(:get)

          described_class.locate_org(user_id)
        end
      end

      context 'valid user id' do
        context 'with related account' do
          it 'should return the correct account' do
            expect(subject).to eq(account_id)
          end
        end

        context 'with no related account' do
          let(:user_id) { user_id_no_account }

          it 'returns nil' do
            expect(subject).to eq(nil)
          end
        end

        context 'that is not in the list' do
          let(:user_id) { 999 }

          it 'returns nil' do
            expect(subject).to eq(nil)
          end
        end
      end

      context 'invalid user id' do
        context 'nil user_id' do
          let(:user_id) { nil }

          it 'returns nil' do
            expect(subject).to eq(nil)
          end
        end
      end
    end
  end
end
