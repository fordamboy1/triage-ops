# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/reaction'

RSpec.describe Triage::Reaction do
  let(:path) { '/a/path' }
  let(:noteable_path) { '/a/noteable/path' }
  let(:body) { 'a body' }
  let(:discussion_id) { 1 }
  let(:discussion_path) { "#{noteable_path}/discussions/#{discussion_id}" }

  describe '#add_comment' do
    it 'adds a new comment' do
      expect(Triage::Reaction).to receive(:post_request).with("#{noteable_path}/notes", body)

      described_class.add_comment(body, noteable_path)
    end
  end

  describe '#add_discussion' do
    it 'adds a new discussion' do
      expect(Triage::Reaction).to receive(:post_request).with("#{noteable_path}/discussions", body)

      described_class.add_discussion(body, noteable_path)
    end
  end

  describe '#append_discussion' do
    it 'posts to an existing discussion' do
      expect(Triage::Reaction).to receive(:post_request).with("#{discussion_path}/notes", body)

      described_class.append_discussion(body, discussion_id, noteable_path)
    end
  end

  describe '#resolve_discussion' do
    it 'resolves an existing discussion' do
      expect(Triage::Reaction).to receive(:put_request).with(discussion_path, resolved: true)

      described_class.resolve_discussion(discussion_id, noteable_path)
    end
  end

  describe '#unresolve_discussion' do
    it 'unresolves an existing discussion' do
      expect(Triage::Reaction).to receive(:put_request).with(discussion_path, resolved: false)

      described_class.unresolve_discussion(discussion_id, noteable_path)
    end
  end

  describe '.post_request' do
    it 'performs a POST request' do
      expect_api_request(verb: :post, path: path, request_body: { body: body }) do
        expect(described_class.post_request(path, body)).to eq("POST #{Triage::PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`")
      end
    end
  end

  describe '.put_request' do
    it 'performs a PUT request' do
      expect_api_request(verb: :put, path: path, request_body: body) do
        expect(described_class.put_request(path, body)).to eq("PUT #{Triage::PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`")
      end
    end
  end
end
