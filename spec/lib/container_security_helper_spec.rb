
# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/container_security_helper'

RSpec.describe ContainerSecurityHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include ContainerSecurityHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'be_missing_status' => { 'departments' => ['Protect:Container Security BE Team'] },
      'be_ooo' => { 'departments' => ['Protect:Container Security BE Team'] },
      'be_not_ooo' => { 'departments' => ['Protect:Container Security BE Team'] },
      'fe_missing_status' => { 'departments' => ['Protect:Container Security FE Team'] },
      'fe_ooo' => { 'departments' => ['Protect:Container Security FE Team'] },
      'fe_not_ooo' => { 'departments' => ['Protect:Container Security FE Team'] }
    }
  end

  let(:roulette) do
    [
      { 'username' => 'be_missing_status' },
      { 'username' => 'be_ooo', 'out_of_office' => true },
      { 'username' => 'be_not_ooo', 'out_of_office' => false },
      { 'username' => 'fe_missing_status' },
      { 'username' => 'fe_ooo', 'out_of_office' => true },
      { 'username' => 'fe_not_ooo', 'out_of_office' => false }
    ]
  end

  before do
    allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
    allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)
  end

  subject { resource_klass.new(labels) }

  describe '#container_security_be' do
    it 'retrieves team members from www-gitlab-com and returns a random container security backend engineer' do
      result = subject.container_security_be

      expect(result).to be_in(%w(@be_not_ooo @be_missing_status))
      expect(result).not_to be_in(%w(@be_ooo @fe_ooo @fe_not_ooo @fe_missing_status))
    end

    context 'when everyone is OOO' do
      let(:team_from_www) do
        {
          'be1' => { 'departments' => ['Protect:Container Security BE Team'] },
          'be2' => { 'departments' => ['Protect:Container Security BE Team'] },
          'be3' => { 'departments' => ['Protect:Container Security BE Team'] },
        }
      end

      let(:roulette) do
        [
          { 'username' => 'be1', 'out_of_office' => true },
          { 'username' => 'be2', 'out_of_office' => true },
          { 'username' => 'be3', 'out_of_office' => true },
        ]
      end

      it 'selects a random team member' do
        expect(subject.container_security_be).to be_in(%w(@be1 @be2 @be3))
      end
    end
  end

  describe '#container_security_fe' do
    it 'retrieves team members from www-gitlab-com and returns a random container security frontend engineer' do
      expect(subject.container_security_fe).to be_in(%w(@fe_ooo @fe_not_ooo @fe_missing_status))
    end
  end
end
