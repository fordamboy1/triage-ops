# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/generate/option'

RSpec.describe Generate::Option do
  describe '.parse' do
    let(:template_path) { 'path/to/template' }
    let(:only_teams) { 'plan,create' }
    let(:assignees) { 'backend_engineering_managers,pms' }

    it 'parses -t to set template' do
      options = described_class.parse(['-t', template_path])

      expect(options.template).to eq(template_path)
    end

    it 'parses --template to set template' do
      options = described_class.parse(['--template', template_path])

      expect(options.template).to eq(template_path)
    end

    it 'parses -o to set only' do
      options = described_class.parse(['-o', only_teams])

      expect(options.only).to eq(only_teams.split(','))
    end

    it 'parses --only to set only' do
      options = described_class.parse(['--only', only_teams])

      expect(options.only).to eq(only_teams.split(','))
    end

    it 'parses -a to set assignee roles' do
      options = described_class.parse(['-a', assignees])

      expect(options.assign).to eq(assignees.split(','))
    end

    it 'parses --assign to set assignee roles' do
      options = described_class.parse(['--assign', assignees])

      expect(options.assign).to eq(assignees.split(','))
    end

    it 'represents a missing assignee as nil' do
      options = described_class.parse(['-t', template_path])

      expect(options.assign).to be_nil
    end
  end
end
