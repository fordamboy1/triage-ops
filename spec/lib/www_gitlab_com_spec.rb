# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/www_gitlab_com'

RSpec.describe WwwGitLabCom, :http_requests_allowed do
  describe '.team_from_www' do
    it 'fetches team from www-gitlab-com data' do
      expect(WwwGitLabCom.team_from_www['sytses'].keys).to include('departments')
    end
  end
end
