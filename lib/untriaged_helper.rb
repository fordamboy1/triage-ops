# frozen_string_literal: true

require_relative 'devops_labels'
require_relative 'team_member_select_helper'

module UntriagedHelper
  include DevopsLabels::Context
  include TeamMemberSelectHelper

  TYPE_LABELS = [
    'type::feature',
    'type::maintenance',
    'type::bug',
    'support request',
    'meta',
    'test',
    'documentation',
    'triage report'
  ].freeze

  def untriaged_issues_triagers
    select_team_member_usernames(include_ooo: false) do |data|
      data['departments']&.any? { |dept| dept.downcase == 'quality department' } &&
        data['role']&.downcase&.include?('engineering manager')
    end
  end

  def merge_request_coaches
    select_team_member_usernames(include_ooo: false) do |data|
      data['departments']&.any? { |dept| dept.downcase == 'merge request coach' }
    end
  end

  def distribute_items(list_items, triagers)
    puts "Potential triagers: #{triagers.inspect}"

    triagers.shuffle
      .zip(list_items.each_slice((list_items.size.to_f / triagers.size).ceil))
      .sort { |(triager_a, _), (triager_b, _)| triager_a <=> triager_b }
      .to_h
      .compact
  end

  def distribute_and_display_items_per_triager(list_items, triagers)
    items_per_triagers = distribute_items(list_items, triagers)

    text = items_per_triagers.each_with_object([]) do |(triager, items), text|
      text << "#{triager}\n\n#{items.join("\n")}"
    end.join("\n\n")

    text + "\n/assign #{items_per_triagers.keys.join(' ')}"
  end

  def merge_requests_from_line_items(items)
    items.lines(chomp: true).map do |item|
      author, url, *labels = item.split(',')
      { author: author, url: url, labels: labels }
    end
  end

  def markdown_table_rows(merge_requests_by_author)
    merge_requests_by_author.map.with_index do |(author, merge_requests), index|
      "| #{index + 1}: #{author} | <ol>#{merge_requests.map { |mr| "<li>#{mr[:url]}: #{mr[:labels].join(' ')}</li>" }.join('') }</ol> |"
    end
  end

  def untriaged?
    !triaged?
  end

  def triaged?
    has_type_label? && has_stage_label? && has_group_label?
  end

  def has_type_label?
    !type_label.nil?
  end

  def type_label
    (label_names & TYPE_LABELS).first
  end
end
